package jwenz723.simplecalculator;


import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;


public class MainFragment extends Fragment {

    private Button btn[] = new Button[10];
    private Button btnOps[] = new Button[4];
    private ImageButton btnBack;
    private Button btnPeriod;
    private Button btnClear;
    private Button btnEquals;
    private TextView txvInput;
    private BigDecimal currentNumber;  // This is used to track what has already been calculated when doing multiple operations on the same number
    private String currentOperator; // This keeps track of what operation is being performed.
    private char timeToClear;   // a variable used to indicate that the textbox needs to be cleared next time a number button is pressed

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        try {
            currentOperator = "";
            timeToClear = 0;
            txvInput = (TextView) rootView.findViewById(R.id.txvInput);
            btnBack = (ImageButton) rootView.findViewById(R.id.btnBack);
            btnPeriod = (Button) rootView.findViewById(R.id.btnPeriod);
            btnClear = (Button) rootView.findViewById(R.id.btnClear);
            btnEquals = (Button) rootView.findViewById(R.id.btnEquals);
            btn[0] = (Button) rootView.findViewById(R.id.btn0);
            btn[1] = (Button) rootView.findViewById(R.id.btn1);
            btn[2] = (Button) rootView.findViewById(R.id.btn2);
            btn[3] = (Button) rootView.findViewById(R.id.btn3);
            btn[4] = (Button) rootView.findViewById(R.id.btn4);
            btn[5] = (Button) rootView.findViewById(R.id.btn5);
            btn[6] = (Button) rootView.findViewById(R.id.btn6);
            btn[7] = (Button) rootView.findViewById(R.id.btn7);
            btn[8] = (Button) rootView.findViewById(R.id.btn8);
            btn[9] = (Button) rootView.findViewById(R.id.btn9);
            btnOps[0] = (Button) rootView.findViewById(R.id.btnPlus);
            btnOps[1] = (Button) rootView.findViewById(R.id.btnMinus);
            btnOps[2] = (Button) rootView.findViewById(R.id.btnMultiply);
            btnOps[3] = (Button) rootView.findViewById(R.id.btnDivide);
        } catch (Exception e) {
            displayError(e);
        }

        // create the onclicklistener to be used by '+', '-', 'x', and '/'
        View.OnClickListener operatorClicker = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    // if there is at least one number entered
                    if (txvInput.getText().toString().length() > 0) {
                        // set the background color of the button that was just clicked, and reset any other buttons
                        updateBackgroundColor(v);

                        // if an ops button has already been pressed
                        if (currentOperator != null && !currentOperator.equals("")) {
                            doMath(v);
                        } else {
                            currentNumber = new BigDecimal(txvInput.getText().toString()); // get the current number inputted
                            currentOperator = ((Button) v).getText().toString(); // set the current operator
                            timeToClear = 1; // set the flag to clear the input on next number click
                        }
                    }
                } catch (Exception e) {
                    displayError(e);
                }
            }
        };

        // create the onclicklistener to be used by the number buttons
        View.OnClickListener numberClicker = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (timeToClear == 1) {
                        txvInput.setText(""); // clear the textbox
                        timeToClear = 0;
                    }

                    txvInput.setText(txvInput.getText() + ((Button) v).getText().toString());
                } catch (Exception e) {
                    displayError(e);
                }
            }
        };

        // set the clicklistener for the operator buttons
        for( int i = 0; i < 4; i++ ) {
            btnOps[i].setOnClickListener(operatorClicker);
        }

        // set the clicklistener for the number buttons
        for( int i = 0; i < 10; i++ ) {
            btn[i].setOnClickListener(numberClicker);
        }

        // period button click listener
        btnPeriod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String curText = txvInput.getText().toString();

                    // if there is not already a period in the number
                    if (!curText.contains(".")) {
                        // append a period
                        txvInput.setText(txvInput.getText() + ".");
                    }
                } catch (Exception e) {
                    displayError(e);
                }
            }
        });

        // backspace click listener
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    int len = txvInput.length();

                    // if there is at least one char to erase
                    if (len > 0) {
                        CharSequence temp = txvInput.getText().subSequence(0, len - 1);
                        txvInput.setText(temp);
                    }
                } catch (Exception e) {
                    displayError(e);
                }
            }
        });

        // clear all entered data
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    txvInput.setText("");
                    currentNumber = null;
                    currentOperator = "";

                    // reset the button colors
                    updateBackgroundColor(v);
                } catch (Exception e) {
                    displayError(e);
                }
            }
        });

        // set equals button listener
        btnEquals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    // if there is at least one number entered and there is a selected operator
                    if (txvInput.getText().toString().length() > 0 && currentOperator.matches("[/\\+x-]{1}")) {
                        doMath(v);

                        // reset the button colors
                        updateBackgroundColor(v);
                    }
                } catch (Exception e) {
                    displayError(e);
                }
            }
        });

        return rootView;

    }

    // function to do the math work
    private void doMath(View v) {
        try {
            BigDecimal result = new BigDecimal("0");
            BigDecimal input = new BigDecimal(txvInput.getText().toString()); // get the text in the text box

            // perform the math
            switch (currentOperator) {
                case "+" :
                    result = currentNumber.add(input);
                    break;
                case "-" :
                    result = currentNumber.subtract(input);
                    break;
                case "x" :
                    result = currentNumber.multiply(input);
                    break;
                case "/" :
                    // limit to 8 decimal places, round up
                    result = currentNumber.divide(input, 8, RoundingMode.HALF_UP);

                    String sResult = result.toString();

                    // while the last char in the number is 0
                    while (sResult.substring(sResult.length() - 1, sResult.length()).equals("0")) {
                        // chop off the last char
                        sResult = sResult.substring(0, sResult.length() - 1);
                    }

                    // if the last char in the string is a decimal point
                    if (sResult.substring(sResult.length() - 1, sResult.length()).equals(".")) {
                        // chop off the decimal
                        sResult = sResult.substring(0, sResult.length() - 1);
                    }

                    result = new BigDecimal(sResult);
                    break;
            }

            // figure out if the equals button was pressed
            if (((Button) v).getText().toString().equals("=")) {
                currentOperator = "";
            } else {
                currentOperator = "=";
            }

            txvInput.setText(result.toString());
            currentNumber = result;
            timeToClear = 1;
        } catch (Exception e) {
            displayError(e);
        }
    }

    // A simple method to display an error message to the user when an exception is thrown
    private void displayError(Exception e) {
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        String exceptionAsString = sw.toString();
        Log.d("Error", exceptionAsString);

        Toast.makeText(getActivity(), "An Error Occurred!", Toast.LENGTH_SHORT).show();
    }

    // sets the color of the clicked operator button to an alternate color and resets all the other
    // operator buttons to default
    private void updateBackgroundColor(View v) {
        // Reset the button colors
        for (Button button : btnOps) {
            button.setBackgroundColor(getResources().getColor(R.color.operator_color));
        }

        // if the clicked button is one of the math operator buttons
        if (((Button)v).getText().toString().matches("[/\\+x-]{1}")) {
            // set the alternate color for the clicked button
            v.setBackgroundColor(getResources().getColor(R.color.abc_search_url_text));
        }
    }
}
