package jwenz723.simplecalculator;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MainFragment mainFragment = new MainFragment();
        // add the fragment to the FrameLayout
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.container, mainFragment);
        transaction.commit(); // causes CourseListFragment to display
    }
}
